from django.contrib import admin
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, Client


ADMIN_ROOT = '/admin/'


# https://tommorris.org/posts/9389
class TestAdmin(TestCase):
    def create_user(self):
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = False
        user.is_active = True
        user.save()
        self.user = user

    def test_admin(self):
        self.create_user()
        client = Client()
        client.login(username=self.username, password=self.password)

        '''
        admin_pages = [
            "/admin/",

            # put all the admin pages for your models in here.
            "/admin/acatto/offer/",
            #"/admin/acatto/institute/",
            #"/admin/acatto/team/",
            #"/admin/acatto/industry/",
            #"/admin/acatto/branch/",
            #"/admin/acatto/person/",
            #"/admin/acatto/product/",
            #"/admin/auth/user/",
            #"/admin/auth/group/",

            "/admin/auth/",
            "/admin/auth/group/",
            "/admin/auth/group/add/",
            "/admin/auth/user/",
            "/admin/auth/user/add/",
            "/admin/password_change/"
        ]
        for page in admin_pages:
        '''

        print()
        page = ADMIN_ROOT
        print(page)
        resp = client.get(page)
        assert resp.status_code == 200

        for page in admin.site.urls[0]:
            page = page.pattern
            if not page._is_endpoint:   # some more clever method how to get admin urls .. ?
                page = eval(page.describe().split(' ', 1)[0])
                page = ADMIN_ROOT + page
                print(page)
                resp = client.get(page)
                assert resp.status_code == 200
                page += 'add/'
                print(page)
                resp = client.get(page)
                assert resp.status_code == 200

class HomeTests(TestCase):
    def test_home_view_status_code(self):
        url = reverse('home')
        resp = self.client.get(url)
        assert resp.status_code == 200
        #self.assertEquals(response.status_code, 200)

'''
"/admin/acatto_x_product_team",
"/admin/acatto_x_product_offer",
"/admin/acatto_x_product_branch",
"/admin/acatto_team_members",
"/admin/acatto_x_product_author",
"/admin/acatto_x_team_author",
'''

